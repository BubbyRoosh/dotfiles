autoload -U colors && colors
PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}% $(hostname -s) %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "
RPROMPT='%(?,%F{green}:%),%F{yellow}%? %F{red}:()%f'
stty stop undef

# Completions
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
# Allow files that start with '.'
_comp_options+=(globdots)

# vi mode
bindkey -v
export KEYTIMEOUT=1

# Save history
HISTFILE=$HOME/.local/.zhistory
HISTSIZE=1000
SAVEHIST=1000
setopt INC_APPEND_HISTORY_TIME

#export JAVA_HOME=/usr/lib/jvm/java-1.8-openjdk/
export JAVA_HOME=/usr/lib/jvm/java-22-openjdk/

[ -f "/home/bubby/.ghcup/env" ] && source "/home/bubby/.ghcup/env" # ghcup-env

#export EDITOR=vim
export EDITOR=nvim

export LANG=en_US.UTF-8

# Chrome on OpenBSD has wasm disabled by default (needed for element)
export ENABLE_WASM=true

# Paths
PATH=$HOME/.local/share:$PATH
PATH=$HOME/.local/bin:$PATH
PATH=$HOME/.local/scripts:$PATH
PATH=$HOME/.cargo/bin:$PATH
PATH=$HOME/go/bin:$PATH

# If yarn
PATH=$HOME/.yarn/bin:$PATH

# If plan9port
#PLAN9=$HOME/Projects/C/plan9
#export PLAN9
#PATH=$PATH:$PLAN9/bin
#export PATH

# If Helix
#export HELIX_RUNTIME=~/.local/src/helix/runtime

# If wayland
if [ -z "$XDG_RUNTIME_DIR" ]; then
	XDG_RUNTIME_DIR="/tmp/$(id -u)-runtime-dir"
	mkdir -pm 0700 "$XDG_RUNTIME_DIR"
	export XDG_RUNTIME_DIR
fi

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

echo -ne '\e[5 q' # Use beam shape cursor on startup. (shows a 'q' when logging in from tty)

[ -e $HOME/.local/scripts/aliasmaker ] && source $HOME/.local/scripts/aliasmaker
