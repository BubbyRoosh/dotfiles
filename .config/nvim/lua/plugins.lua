local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
    {
        "marko-cerovac/material.nvim",
        init = function()
            vim.g.material_style = "deep ocean"
            vim.cmd [[ colorscheme material ]]
            require("material").setup({
                plugins = {
                    "nvim-cmp",
                    "sneak",
                    "telescope",
                },
            })
        end,
    },
    {
        "nvim-lualine/lualine.nvim",
        init = function()
            require("lualine").setup({
                options = {
                    theme = "material"
                }
            })
        end,
    },
    --{
    --	"https://codeberg.org/BubbyRoosh/bubbytheme",
    --	init = function()
    --		vim.cmd [[ colorscheme bubby ]]
    --	end,
    --},
    {
        "neovim/nvim-lspconfig",
        event = "VeryLazy",
        dependencies = {
            "hrsh7th/cmp-nvim-lsp",
            "hrsh7th/cmp-buffer",
            "hrsh7th/cmp-path",
            "hrsh7th/cmp-cmdline",
            "hrsh7th/nvim-cmp",
            "hrsh7th/cmp-vsnip",
            "hrsh7th/vim-vsnip",
        },
    },
    {
        "nvim-telescope/telescope.nvim",
        dependencies = {
            "nvim-lua/plenary.nvim",
        },
    },
    {
        "justinmk/vim-sneak",
        event = "VeryLazy",
    },
    {
        "mg979/vim-visual-multi",
    },
    {
        'windwp/nvim-autopairs',
        event = "InsertEnter",
        config = true
    },
    {
        "nvim-treesitter/nvim-treesitter",
        init = function()
            --local parser_dir = os.getenv("HOME") .. "/.local/share/nvim/parsers/"
            --vim.opt.runtimepath:append(parser_dir)
            require("nvim-treesitter.configs").setup({
                --parser_install_dir = parser_dir,
                --auto_install = true,
                highlight = {
                    enable = true,
                },
            })
        end,
    },
})
