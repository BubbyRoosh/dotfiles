vim.g.mapleader = ' '
vim.opt.colorcolumn = "80"
vim.opt.number = true

vim.opt.tabstop = 4
vim.opt.expandtab = true
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4

vim.opt.termguicolors = true
