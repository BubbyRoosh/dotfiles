#!/bin/sh
yellow="\x1b[1;33m"
magenta="\x1b[1;35m"
white="\x1b[1;37m"
bold="\x1b[1m"
clear="\x1b[m"


os=$(uname)

out() {
	printf '%b-> %b%s%s\n' "$yellow" "$clear" "$1" "$2"
}

gethostmachine() {
	case $os in
		Linux) cat /sys/devices/virtual/dmi/id/product_family ;;
                *BSD) sysctl -n hw.vendor hw.product | tr '\n' ' ' ;;
	esac
}

getuptime() {
	case $os in
		Linux) IFS=. read -r s _ < /proc/uptime ;;
		OpenBSD) s=$(( $(date +%s) - $(sysctl -n kern.boottime) )) ;;
	esac
	
	days=$((s / 60 / 60 / 24))
	hours=$((s / 60 / 60 % 24))
	minutes=$((s / 60 % 60))

	[ "$days" -gt 0 ] && uptime="${days}d "
	[ "$hours" -gt 0 ] && uptime="${uptime}${hours}h "
	[ "$minutes" -gt 0 ] && uptime="${uptime}${minutes}m "
	printf "$uptime"
}

getpkgs() {
	pkgs=""

	if [ $(command -v kiss) ]
	then
		pkgs="$(kiss l | wc -l) (kiss)"
	fi

	if [ $(command -v pacman) ]
	then
		pkgs="$pkgs $(pacman -Q | wc -l) (pacman)"
	fi

	if [ $(command -v emerge) ]
	then
		pkgs="$pkgs $(ls -d /var/db/pkg/*/* | wc -l) (portage)"
	fi

	if [ $(command -v xbps-query) ]
	then
		pkgs="$pkgs $(xbps-query -l | wc -l) (xbps)"
	fi

	if [ $(command -v apk) ]
	then
		pkgs="$pkgs $(apk info | wc -l) (apk)"
	fi

	if [ $(command -v pkg) ]
	then
		pkgcnt=$(pkg info | wc -l)
		# Need to remove whitespace at the beginning cause FreeBSD moment
		pkgs="$pkgs $(echo $pkgcnt | sed -e 's/ //g') (pkg)"
	fi

	if [ $(command -v pkg_info) ]
	then
		pkgcnt=$(pkg_info | wc -l)
		pkgs="$pkgs $(echo $pkgcnt | sed -e 's/ //g') (pkg)"
	fi
	pkgs=$(printf "$pkgs" | sed -e 's/^ //g')
	printf "$pkgs"
}

getmem() {
	case $os in
		Linux) printf $(free -m | awk 'FNR==2 {print $3"M/"$2"M"}') ;;
		OpenBSD) 
			total=$(( $(sysctl -n hw.physmem) / 1024 / 1024 ))
			used=$(vmstat | awk 'END {print $3}')
			printf "%s/%sM" $used $total
			;;
	esac
}

[ -f /etc/os-release ] && source /etc/os-release

getos() {
	case $os in
		Linux) printf "$PRETTY_NAME" ;;
		*) printf $os ;;
	esac
}

# Moment
printf '%b%s%b%b%s%b%b%s\n' "$magenta" "$(whoami)" "$white" "$bold" "@" "$clear" "$magenta" "$(hostname)"


out "os" "         $(getos)"
out "host" "       $(gethostmachine)"
out "kernel" "     $(uname -sr)"
out "uptime" "     $(getuptime)"
out "pkg" "        $(getpkgs)"
out "sh" "         ${SHELL##*\/}"
out "mem" "        $(getmem)"
